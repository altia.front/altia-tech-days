import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Inicio',
    icon: 'nb-home',
    link: '/pages/inicio',
    home: true,
  },
  {
    title: 'Módulos',
    icon: 'nb-layout-sidebar-right',
    link: '/pages/modules',
    home: false,
  },
  {
    title: 'Data binding',
    icon: 'nb-loop',
    link: '/pages/data-binding',
    home: false,
  },
  {
    title: 'Servicios',
    icon: 'nb-list',
    link: '/pages/services',
    home: false,
  },
  {
    title: 'Posts/Directivas',
    icon: 'nb-compose',
    link: '/pages/posts',
    home: false,
  },
  {
    title: 'Fotos',
    icon: 'nb-grid-b-outline',
    link: '/pages/fotos',
    home: false,
  },
  // {
  //   title: 'FEATURES',
  //   group: true,
  // },
  // {
  //   title: 'Auth',
  //   icon: 'nb-locked',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
