import { DataBindingModule } from './data-binding/data-binding.module';
import { ServicesModule } from './services/services.module';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PostsModule } from './posts/posts.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { FotosModule } from './fotos/fotos.module';
import { InicioModule } from './inicio/inicio.module';
import { ModulesModule } from './modules/modules.module';
import { NbToastrService } from '@nebular/theme';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    PostsModule,
    FotosModule,
    InicioModule,
    ModulesModule,
    ServicesModule,
    DataBindingModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
  providers: [
    NbToastrService
  ]
})
export class PagesModule {
}
