import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { PostsComponent } from './posts.component';
import { MarcarModule } from '../../directives/marcar.directive.module';

@NgModule({
  imports: [
    ThemeModule,
    // MarcarModule
  ],
  declarations: [
    PostsComponent
  ],
})
export class PostsModule { }
