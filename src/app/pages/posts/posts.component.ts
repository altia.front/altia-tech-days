import { Component, OnInit } from '@angular/core';
import { PostsModel } from '../../models/posts/posts.interface';
import { ApiService } from '../../providers/api/api.service';

@Component({
  selector: 'ngx-posts',
  templateUrl: './posts.component.html',
})
export class PostsComponent implements OnInit {

  posts: Array<PostsModel>;

  constructor(private apiSrvc: ApiService) {

  }

  ngOnInit(): void {
    this.apiSrvc.getPosts().subscribe((_data) => {
      console.log('Posts obtenidos correctamente:', _data);
      this.posts = _data;
    })
  }

}
