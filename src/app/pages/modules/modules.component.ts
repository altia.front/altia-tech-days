import { FotoModel } from './../../models/foto/foto';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../providers/api/api.service';

@Component({
  selector: 'ngx-modules',
  styleUrls: ['./modules.component.scss'],
  templateUrl: './modules.component.html',
})
export class ModulesComponent implements OnInit {

  fotos: Array<FotoModel>;

  constructor(private apiSrvc: ApiService) {

  }

  ngOnInit(): void {
    this.apiSrvc.getFotos().subscribe((_data) => {
      console.log('Fotos obtenidas correctamente:', _data);
      this.fotos = _data;
    }, (_err) => {
      console.error('Ha ocurrido un error: ', _err);
    });
  }

}
