import { ModuleComponentComponent } from './module-component/module-component.component';
import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { ModulesComponent } from './modules.component';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    ModulesComponent,
    ModuleComponentComponent
  ],
})
export class ModulesModule { }
