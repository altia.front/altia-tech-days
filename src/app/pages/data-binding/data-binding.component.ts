import { Component } from '@angular/core';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';

@Component({
  selector: 'ngx-data-binding',
  styleUrls: ['./data-binding.component.scss'],
  templateUrl: './data-binding.component.html',
})
export class DataBindingComponent {

  user: { name: string, surname: string } = {
    name: 'Juanma',
    surname: 'Macias'
  };

  constructor(private toastrService: NbToastrService) {

  }

  showUser() {
    let config = {
      status: NbToastStatus.WARNING,
      destroyByClick: true,
      duration: 9000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: true,
    };
    let titleContent = `Nombre: ${this.user.name} , Apellido: ${this.user.surname}`;

    this.toastrService.show(
      `${titleContent}`,
      `Usuario introducido`,
      config);
  }

  resetUsername() {
    this.user = {
      name: '',
      surname: ''
    };
  }

}
