import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { DataBindingComponent } from './data-binding.component';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    DataBindingComponent
  ],
})
export class DataBindingModule { }
