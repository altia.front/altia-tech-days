import { Component } from '@angular/core';

@Component({
  selector: 'ngx-components',
  styleUrls: ['./inicio.component.scss'],
  templateUrl: './inicio.component.html',
})
export class InicioComponent {

  constructor() {

  }

  getBckImg() {
    let style = {
      'background-image': `url(./assets/images/altia-techdays.png)`
    }
    return style;
  }

}
