import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { InicioComponent } from './inicio.component';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    InicioComponent
  ],
})
export class InicioModule { }
