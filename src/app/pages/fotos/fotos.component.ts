import { FotoModel } from './../../models/foto/foto';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../providers/api/api.service';

@Component({
  selector: 'ngx-fotos',
  styleUrls: ['./fotos.component.scss'],
  templateUrl: './fotos.component.html',
})
export class FotosComponent implements OnInit {

  fotos: Array<FotoModel>;

  constructor(private apiSrvc: ApiService) {

  }

  ngOnInit(): void {
    this.apiSrvc.getFotos().subscribe((_data) => {
      console.log('Fotos obtenidas correctamente:', _data);
      this.fotos = _data;
    }, (_err) => {
      console.error('Ha ocurrido un error: ', _err);
    });
  }

  getBckImg(url: string) {
    let style = {
      'background-image': `url(${url})`
    }
    return style;
  }

}
