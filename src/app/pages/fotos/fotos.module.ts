import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { FotosComponent } from './fotos.component';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    FotosComponent,
  ],
})
export class FotosModule { }
