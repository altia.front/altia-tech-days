import { Component, OnDestroy, OnInit, Input } from "@angular/core";
import { CountdownService } from "../../../providers/countdown/countdown.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'ngx-countdown',
  templateUrl: './countdown.component.html',
  // providers: [CountdownService]
})
export class CountdownComponent implements OnDestroy, OnInit {

  // Atributo de entrada
  @Input('time') time: number;

  countDownStatus: number;
  countDwnSubscription: Subscription;

  constructor(private countdownSrvc: CountdownService) {
    this.countDwnSubscription = this.countdownSrvc.onDecrease.subscribe((_data) => {
      this.countDownStatus = _data;
    });
  }

  ngOnInit(): void {
    this.countdownSrvc.startCountdown(this.time);
  }

  ngOnDestroy(): void {
    this.countDwnSubscription.unsubscribe();
  }
}
