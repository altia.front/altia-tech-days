import { ServicesComponent } from './services.component';
import { NgModule } from '@angular/core';


import { ThemeModule } from '../../@theme/theme.module';
import { CountdownComponent } from './countdown/countdown.component';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    ServicesComponent,
    CountdownComponent
  ],
})
export class ServicesModule { }
