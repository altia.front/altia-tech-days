import { DataBindingComponent } from './data-binding/data-binding.component';
import { ModulesComponent } from './modules/modules.component';
import { FotosComponent } from './fotos/fotos.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PostsComponent } from './posts/posts.component';
import { InicioComponent } from './inicio/inicio.component';
import { ServicesComponent } from './services/services.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'posts',
      component: PostsComponent,
    },
    {
      path: 'inicio',
      component: InicioComponent,
    },
    {
      path: 'fotos',
      component: FotosComponent,
    },
    {
      path: 'services',
      component: ServicesComponent,
    },
    {
      path: 'modules',
      component: ModulesComponent,
    },
    {
      path: 'data-binding',
      component: DataBindingComponent,
    },
    {
      path: '',
      redirectTo: 'inicio',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
