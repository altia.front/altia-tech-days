import { Injectable, EventEmitter } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable(
  // { providedIn: 'root' }
)
export class CountdownService {

  onDecrease = new EventEmitter<number>();
  onComplete = new EventEmitter<void>();

  public counter: number = 0;
  private countdownTimerRef: any = null;

  constructor() { }

  startCountdown(init: number) {
    if (init && init > 0) {
      this.clearTimeout();
      this.counter = init;
      this.doCountdown();
    }
  }

  doCountdown() {
    this.countdownTimerRef = setTimeout(() => {
      this.counter = this.counter - 1;
      this.processCountdown();
    }, 1000);
  }

  private clearTimeout() {
    if (this.countdownTimerRef) {
      clearTimeout(this.countdownTimerRef);
      this.countdownTimerRef = null;
    }
  }

  processCountdown() {
    this.onDecrease.emit(this.counter);
    console.log("count is ", this.counter);

    if (this.counter == 0) {
      this.onComplete.emit();
      console.log("--counter end--");
    }
    else {
      this.doCountdown();
    }
  }


}
