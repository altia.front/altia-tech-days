import { FotoModel } from './../../models/foto/foto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PostsModel } from '../../models/posts/posts.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable(
  // { providedIn: 'root' }
)
export class ApiService {

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Array<PostsModel>> {
    return this.http.get<Array<PostsModel>>('https://jsonplaceholder.typicode.com/posts');
  }

  getFotos(): Observable<Array<FotoModel>> {
    return this.http.get<Array<FotoModel>>('https://jsonplaceholder.typicode.com/photos')
      .pipe(
        map((_data) => {
          return _data.splice(0, 20);
        })
      );

  }

}
