import { ThemeModule } from "../@theme/theme.module";
import { NgModule } from "@angular/core";
import { MarcarDirective } from "./marcar.directive";

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    MarcarDirective
  ],
  exports: [
    MarcarDirective
  ]
})
export class MarcarModule { }
