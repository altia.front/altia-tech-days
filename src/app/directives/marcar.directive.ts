import { Directive, Input, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[marcar]'
})
export class MarcarDirective {

  @Input('marcar') color: string = 'orange';

  backgroundColor: string;
  defaultColor: string = 'white';

  constructor() { }

  @HostListener('mouseenter') onMouseEnter() {
    this.backgroundColor = this.color;
  }

  @HostListener('mouseout') onMouseOut() {
    this.backgroundColor = this.defaultColor;
  }

  @HostBinding('style.backgroundColor') get getColor() {
    return this.backgroundColor;
  }

}
